// Поиск
const searchBtn = document.querySelector('.search-btn');
const searchContent = document.querySelector('.search');

searchBtn.addEventListener('click', function (ev) {
  ev.stopPropagation();

  searchContent.classList.add('search--active')

  document.addEventListener('click', function (e) {

    const inArea = e.composedPath().includes(searchContent || searchBtn);

    if (!inArea) {
      searchContent.classList.remove('search--active');
    }
  });

  document.addEventListener('keydown', function (esc) {
    if (esc.key === 'Escape') {
      searchContent.classList.remove('search--active');
    }
  });
});



//Вход
const logInBtn = document.querySelector('.login-btn');
const login = document.querySelector('.log-in');

const header = document.querySelector('.header');
const main = document.querySelector('.main');
const closeBtn = document.querySelector('.log-in-close')

var modal = document.querySelector('.log-in');
var lastFocusedElement;

function modalShow() {
  lastFocusedElement = document.activeElement;
  modal.focus();
}

function removeModal() {
  lastFocusedElement.focus();
}

logInBtn.addEventListener('click', function () {
  login.classList.add('log-in--active');
  header.classList.add('class--shadow');
  main.classList.add('class--shadow');
  document.body.classList.add('stop-scroll');

  modalShow();

  closeBtn.addEventListener('click', function () {
    login.classList.remove('log-in--active');
    header.classList.remove('class--shadow');
    main.classList.remove('class--shadow');
    document.body.classList.remove('stop-scroll');

    removeModal();
  });

  window.addEventListener('keydown', function (esc) {

    if (esc.key === 'Escape') {
      login.classList.remove('log-in--active');
      header.classList.remove('class--shadow');
      main.classList.remove('class--shadow');
      document.body.classList.remove('stop-scroll');

      removeModal();
    }
  })
});

// Валидатор логин
const validation = new JustValidate('.log-in__form', {
  errorLabelStyle: {
    color: '#D52B1E',
    fontSize: '12px',
    lineHeight: '12px',
    paddingLeft: '32px',
    top: '9px',
    position: 'absolute'
  },
  errorFieldCssClass: 'is-invalid',
});

validation
  .addField('#name', [
    {
      rule: 'minLength',
      value: 3,
      errorMessage: 'Ошибка'
    },
    {
      rule: 'maxLength',
      value: 30,
      errorMessage: 'Ошибка'
    },
    {
      rule: 'required',
      errorMessage: 'Ошибка',
    },
  ])
  .addField('#password', [
    {
      rule: 'required',
      errorMessage: 'Ошибка',
    },
    {
      rule: 'password',
      errorMessage: 'Ошибка',
    },
  ]);

//Валидатор формы

const validate = new JustValidate('.about__form', {
  errorLabelStyle: {
    color: '#D52B1E',
    fontSize: '12px',
    lineHeight: '12px',
    paddingLeft: '32px',
    top: '9px',
    position: 'absolute'
  },
  errorFieldCssClass: 'is-invalid',
});

validate
  .addField('#about-text', [
    {
      rule: 'required',
      errorMessage: 'Ошибка'
    },
  ])
  .addField('#about-name', [
    {
      rule: 'required',
      errorMessage: 'Ошибка',
    },
  ])
  .addField('#about-mail', [
    {
      rule: 'required',
      errorMessage: 'Ошибка',
    },
  ]);

var form = document.querySelector('.about__form');

form.addEventListener('submit', function (event) {
  if (!event.target.checkValidity()) {
    event.preventDefault();
    var inputFields = form.querySelectorAll('input');
    for (let i = 0; i < inputFields.length; i++) {
      if (!inputFields[i].validity.valid) {
        inputFields[i].focus();
        return false;
      }
    }
  }
}, false);


// Плавный переход
const anchors = document.querySelectorAll('.nav__list a[href*="#"]');

for (let anchor of anchors) {
  anchor.addEventListener('click', function (e) {
    e.preventDefault();

    const sectionID = anchor.getAttribute('href').substr(1);

    document.getElementById(sectionID).scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    });
  });
}

// Кнопка показать еще
const moreBtn = document.querySelector('.more__btn');
const podcastItem = document.querySelectorAll('.podcasts__item');

moreBtn.addEventListener('click', function () {
  podcastItem.forEach(el => {
    el.classList.add('podcasts__item--visible');
  });
  moreBtn.closest('.podcasts__more').classList.add('podcasts__more--hidden');
});

// Селект
const select = document.querySelector('.broadcast__select');
const choices = new Choices(select, {
  itemSelectText: '',
  shouldSort: false,
  searchEnabled: false,
  position: 'bottom',
});

// Аккордеон
$('.accordion').accordion({
  heightStyle: 'content',
  active: false
});

const accordionBtn = document.querySelectorAll('.guests-item__container');
const accordionList = document.querySelector('.guests__list');



var btnArray = Array.from(accordionBtn);

accordionBtn[0].lastElementChild.classList.add('guests-item__btn--active');

for (let i = 0; i < btnArray.length; i++) {

  btnArray[i].setAttribute('tabindex', '0');

  btnArray[i].addEventListener('click', function (e) {

    if (!e.target.lastElementChild.classList.contains('guests-item__btn--active')) {
      accordionBtn[0].lastElementChild.classList.remove('guests-item__btn--active');
      e.target.lastElementChild.classList.add('guests-item__btn--active');
    }

    accordionList.addEventListener('click', function (ev) {

      const inArea = ev.composedPath().includes(e.target.parentElement);

      if (!inArea && e.target.lastElementChild.classList.contains('guests-item__btn--active')) {
        e.target.lastElementChild.classList.remove('guests-item__btn--active');
      }
    })
  })
}

for (let j = 0; j <= btnArray.length - 1; j++) {

  btnArray[j].addEventListener('keydown', function (eventKey) {
    if (eventKey.key === 'Enter') {

      console.dir(eventKey.target)

      eventKey.target.lastElementChild.classList.add('guests-item__btn--active');
      accordionList.addEventListener('keydown', function (evKey) {
        if (evKey.key === 'Enter') {

          const inArea = evKey.composedPath().includes(eventKey.target.parentElement);

          if (!inArea && eventKey.target.lastElementChild.classList.contains('guests-item__btn--active')) {
            eventKey.target.lastElementChild.classList.remove('guests-item__btn--active');
          }
        }
      })
    }
  })
}

// Гости
const guestTab = document.querySelectorAll('.guests-content__item');
const guestImg = document.querySelector('.guests__img');
const guestContent = document.querySelector('.guests-right__content');
const guestsName = document.querySelector('.guests__name');
const guestDescr = document.querySelector('.guests__description')

guestTab.forEach(tab => {
  tab.addEventListener('click', function (e) {
    guestImg.srcset = e.target.dataset.src;

    guestContent.classList.add('guests-right__content--active');
    guestsName.innerHTML = `${e.target.innerHTML}`;
    guestDescr.innerHTML = `${e.target.firstElementChild.innerHTML}`;
  })

  tab.addEventListener('keydown', function (event) {

    if (event.key === 'Enter') {
      event.target.parentElement.parentElement.firstElementChild.lastElementChild.classList.add('guests-item__btn--active')

      guestImg.srcset = event.target.dataset.src;

      guestContent.classList.add('guests-right__content--active');
      guestsName.innerHTML = `${event.target.innerHTML}`;
      guestDescr.innerHTML = `${event.target.firstElementChild.innerHTML}`;
    }
  })
});

//SWIPER

const swiper = new Swiper('.swiper', {
  loop: true,

  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  slidesPerView: '4',
  breakpoints: {
    1920: {
      slidesPerView: '4',
      spaceBetween: 30,
    },
    1366: {
      slidesPerView: '4',
    },
    992: {
      slidesPerView: '2',
      spaceBetween: 30,
    },

    576: {
      slidesPerView: '2',
      spaceBetween: 30,
    },

    320: {
      slidesPerView: 'auto',
      spaceBetween: 20,
    },
  }
});

const swiperRadio = document.querySelector('.swiper-radio');

let mySwiper;

function mobileSlider() {
  if (window.innerWidth <= 576 && swiperRadio.dataset.mobile == 'false') {

    mySwiper = new Swiper(swiperRadio, {
      spaceBetween: 57,
      slidesPerView: 'auto',
    });
    swiperRadio.dataset.mobile = 'true';
  }

  if (window.innerWidth > 576) {
    swiperRadio.dataset.mobile = 'false';

    if (swiperRadio.classList.contains('swiper-initialized')) {
      mySwiper.destroy();
    }
  }
}

mobileSlider();

window.addEventListener('resize', () => {
  mobileSlider();
})

// кнопка disabled

const checkbox = document.querySelector('.about__checkbox');
const checkBtn = document.querySelector('.about__btn');

checkbox.addEventListener('click', function () {
  if (checkbox.checked == false) {
    checkBtn.disabled = true;
  } else if (checkbox.checked == true) {
    checkBtn.disabled = false;
  }
})

// BURGER MENU

const burgerBtn = document.querySelector('.burger');
const menu = document.querySelector('.header-top__nav');
let menuLinks = document.querySelectorAll('.nav__link');
const menuBottom = document.querySelector('.header-bottom__nav');

if (document.documentElement.clientWidth < 577) {

  menuShow();

  burgerBtn.addEventListener('click', function () {
    burgerBtn.classList.toggle('burger--active');
    menu.classList.toggle('header-top__nav--active');
    menuBottom.classList.toggle('header-bottom__nav--active');

    document.body.classList.toggle('stop-scroll');

    window.addEventListener('keydown', function (esc) {
      if (esc.key === 'Escape') {
        logInBtn.setAttribute('tabindex', '0');

        burgerBtn.classList.remove('burger--active');
        menu.classList.remove('header-top__nav--active');
        menuBottom.classList.remove('header-bottom__nav--active');
        document.body.classList.remove('stop-scroll');
      }
    })
  });

  menuLinks.forEach(function (el) {
    el.addEventListener('click', function () {
      burgerBtn.classList.remove('burger--active');
      menu.classList.remove('header-top__nav--active');
      menuBottom.classList.remove('header-bottom__nav--active');
      document.body.classList.remove('stop-scroll');
    })
  })

} else {
  burgerBtn.addEventListener('click', function () {
    burgerBtn.classList.toggle('burger--active');
    menu.classList.toggle('header-top__nav--active');

    document.body.classList.toggle('stop-scroll');
  });

  menuLinks.forEach(function (el) {
    el.addEventListener('click', function () {
      burgerBtn.classList.remove('burger--active');
      menu.classList.remove('header-top__nav--active');
      document.body.classList.remove('stop-scroll');
    })
  })
}

function menuShow() {
  menu.setAttribute('tabindex', '0');
  menuBottom.setAttribute('tabindex', '0');
  logInBtn.setAttribute('tabindex', '-1');

  menu.focus();
  menuBottom.focus();
}

// Кнопка в эфире

const air = document.querySelector('.air');
const btnPlay = document.querySelectorAll('.header-bottom__btn')

air.addEventListener('click', function () {
  air.classList.toggle('air--active');
  btnPlay.forEach(element => {
    element.classList.toggle('header-bottom__btn--active');
  })
})
